'use strict'

const dynogels = require('dynogels-promisified');
const Joi = require('joi');

const trialTableName = process.env.TRIAL_TABLE_NAME;

const Trial = dynogels.define('Trial', {
    tableName: trialTableName,
    hashKey : 'id',
    timestamps : true,
    schema : {
        id: dynogels.types.uuid(),
        name: Joi.string()
    }

});

module.exports = Trial;