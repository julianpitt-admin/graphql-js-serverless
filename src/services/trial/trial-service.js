'use strict'

const Trial = require('./trial');

const getAllTrials = () => {
    return Trial
        .scan()
        .loadAll()
        .execAsync()
        .then(result => dynogelToArray(result));
};

const getTrial = (id) => {
    console.log('in trial service getTrial', id);
    return Trial.getAsync(id)
        .then(result => dynogelToArray(result));
};

const createTrial = (trialObject) => {
    const mapping = map(trialObject);
    return Trial
        .createAsync(mapping)
        .then(result => dynogelToArray(result));
};

const removeTrial = (id) => {
    return Trial.destroyAsync({'id': id})
        .then(result => dynogelToArray(result));
};

const updateTrial = (trialObject) => {
    const mapping = map(trialObject);
    return Trial.updateAsync(mapping)
        .then(result => dynogelToArray(result));
};

const dynogelToArray = (dynogelResult) => {
    
    if(!dynogelResult) {
        return dynogelResult;
    }

    if(dynogelResult.hasOwnProperty('Items')) {
        return dynogelResult.Items.map((item) => {
            return item.attrs;
        });
    } else {
        return dynogelResult.attrs;
    }

};

const map = (raw) => {
    
    const allowed = [
        'id',
        'name'
    ];

    return Object.keys(raw)
        .filter(key => allowed.includes(key))
        .reduce((obj, key) => {
        obj[key] = raw[key];
        return obj;
        }, {});
};

module.exports = {
    createTrial,
    getAllTrials,
    getTrial,
    removeTrial,
    updateTrial,
    map
}