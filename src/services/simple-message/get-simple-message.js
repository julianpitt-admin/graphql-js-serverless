'use strict'

module.exports.handler = function(event, context, cb) {

    const messageReceived = { 
        returnedMessage: `You said: ${event.message}`
    };
    
    cb(null, messageReceived);
    
}