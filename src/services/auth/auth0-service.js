'use strict'

const request = require('request-promise');
const jwt = require('jsonwebtoken');
const jwksClient = require('jwks-rsa');
const authConfig = JSON.parse(process.env.AUTH_CONFIG);

const login = (username, password) => {

    var options = { method: 'POST',
    url: authConfig.url,
    headers: { 'content-type': 'application/json' },
    body:
    {
        "grant_type": "password",
        "username": username,
        "password": password,
        "client_id": authConfig.client_id,
        "client_secret": authConfig.client_secret,
        "audience": authConfig.audience
      },
    json: true };
    
    return request.post(options);

};

const validate = (event) => {
    return new Promise((resolve, reject) => {
        try {
            // console.log('config', authConfig);

            const authHeader = event.headers.Authorization;
            const client = jwksClient({
                strictSsl: true, // Default value
                jwksUri: authConfig.jwksUrl
            });
            
            const token = authHeader.replace('bearer ', '');
            
            const options = {
                algorithms: ["RS256"]
            };
            
            const kid = jwt.decode(token, {complete: true}).header.kid;
            
            client.getSigningKey(kid, (err, key) => {
                if (err) {
                    console.log('signing key error');
                    console.log(err);
                        resolve(null);
                        return;
                }

                const signingKey = key.publicKey || key.rsaPublicKey;
            
                jwt.verify(token, signingKey, options, (err, decoded) => {
                    if (err) {
                        console.log('Unauthorized');
                        console.log(err);
                        resolve(null);
                        return
                    } else {
                        console.log('Authorized');
                        resolve(true);
                        return;
                    }
                });
            });
        } catch (err) {
            console.error(err);
            resolve(null);
            return;
        }
    });
}

module.exports = {
    login,
    validate
}
