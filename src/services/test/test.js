'use strict'

const dynogels = require('dynogels-promisified');
const Joi = require('joi');

const testTableName = process.env.TEST_TABLE_NAME;

const Test = dynogels.define('Test', {
    tableName: testTableName,
    hashKey : 'id',
    timestamps : true,
    schema : {
        id: dynogels.types.uuid(),
        name: Joi.string(),
        description: Joi.string(),
        trialId: dynogels.types.uuid()
    },
    indexes : [{
      hashKey : 'trialId', name : 'TrialIndex', type : 'global'
    }]

});

module.exports = Test;