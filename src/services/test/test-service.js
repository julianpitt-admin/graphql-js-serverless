'use strict'

const Test = require('./test');

const getTestsByTrialId = (trialId) => {
    return Test
        .query(trialId)
        .usingIndex('TrialIndex')
        .execAsync()
        .then(result => dynogelToArray(result));
};

const getAllTests = () => {
    return Test
        .scan()
        .loadAll()
        .execAsync()
        .then(result => dynogelToArray(result));
};

const getTest = (id) => {
    return Test.getAsync(id)
        .then(result => dynogelToArray(result));
};

const createTest = (testObject) => {
    const mapping = map(testObject);
    return Test
        .createAsync(mapping)
        .then(result => dynogelToArray(result));
};

const removeTest = (id) => {
    return Test.destroyAsync({'id': id})
        .then(result => dynogelToArray(result));
};

const updateTest = (testObject) => {
    const mapping = map(testObject);
    return Test.updateAsync(mapping)
        .then(result => dynogelToArray(result));
};

const dynogelToArray = (dynogelResult) => {
    console.log('Raw result ', dynogelResult);
    
    if(!dynogelResult) {
        return dynogelResult;
    }

    if(dynogelResult.hasOwnProperty('Items')) {
        return dynogelResult.Items.map((item) => {
            return item.attrs;
        });
    } else {
        return dynogelResult.attrs;
    }

};

const map = (raw) => {
    
    const allowed = [
        'id',
        'name',
        'description',
        'trialId'
    ];

    return Object.keys(raw)
        .filter(key => allowed.includes(key))
        .reduce((obj, key) => {
        obj[key] = raw[key];
        return obj;
        }, {});
};

module.exports = {
    createTest,
    getAllTests,
    getTest,
    removeTest,
    updateTest,
    getTestsByTrialId,
    map
}