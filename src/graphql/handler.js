'use strict'

const graphql = require('graphql');
const schema = require('./services/service-loader');
const authorizer = require('./auth/authorizer');
const dataloader = require('./util/dataloader');

const runQuery = (query, claims, variables) => {

    if (claims) {
        console.log('Authenticated');
        return graphql.graphql(schema.authenticated(dataloader), query, {claims: claims}, {dataloader: dataloader}, variables);
    }

    console.log('Not authenticated');
    return graphql.graphql(schema.public(dataloader), query, {}, {dataloader: dataloader}, variables);

}

module.exports.handler = function(event, context, callback) {

    // console.log('Event: ', JSON.stringify(event, null, 2));

    const request = JSON.parse(event.body);

    // console.log('Query: ' + JSON.stringify(request.query));
    // console.log('Variables: ' + JSON.stringify(request.variables));

    return authorizer.authorize(event)
    .then((userInfo) => {
        // console.log('User info', userInfo);
        return runQuery(request.query, userInfo, request.variables);
    })
    .then(result => {

        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
            },
            body: JSON.stringify(result)
        }
        
        return response;

    })
    .then(response => callback(null, response))
    .catch(err => callback(err));

};