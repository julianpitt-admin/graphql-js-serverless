'use strict'

const {makeExecutableSchema} = require('graphql-tools');

const services = [
    'test',
    'trial',
    'auth0',
    'simple-message'
];

const getSchemaAndResolvers = (privateSchema, dataloader) => {

    let schema = {
        types: '',
        queries: '',
        mutations: '',
        resolvers: {
            Query: {
            },
            Mutation: {
            }
        }
    };

    services.map((serviceLocation) => {
        
        const service = require('./service-definitions/' + serviceLocation);
    
        if(service.hasOwnProperty('public')) {
            
            // Building the schema 

            if(service.public.Schema.hasOwnProperty('types')) {
                schema.types += service.public.Schema.types;
            }
            if(service.public.Schema.hasOwnProperty('queries')) {
                schema.queries += service.public.Schema.queries;
            }
            if(service.public.Schema.hasOwnProperty('mutations')) {
                schema.mutations += service.public.Schema.mutations;
            }
            

            // Building the resolvers

            if(service.public.Query) {
                schema.resolvers.Query = Object.assign(schema.resolvers.Query, service.public.Query);
            }
            if(service.public.Mutation) {
                schema.resolvers.Mutation = Object.assign(schema.resolvers.Mutation, service.public.Mutation);    
            }
            if(service.public.Types) {
                Object.keys(service.public.Types).map((type) => {
                    schema.resolvers[type] = service.public.Types[type];
                });
            }
            if(service.public.Dataloaders) {
                Object.keys(service.public.Dataloaders).map((dlKey) => {
                    console.log('Adding dataloader', dlKey);
                    dataloader.defineLoader(dlKey, service.public.Dataloaders[dlKey]);
                });
            }
        }


        if(privateSchema) {

            if(service.hasOwnProperty('private')) {
                
                if(service.private.Schema.hasOwnProperty('types')) {
                    schema.types += service.private.Schema.types;
                }
                if(service.private.Schema.hasOwnProperty('queries')) {
                    schema.queries += service.private.Schema.queries;
                }
                if(service.private.Schema.hasOwnProperty('mutations')) {
                    schema.mutations += service.private.Schema.mutations;
                }
                
                if(service.private.Query) {
                    schema.resolvers.Query = Object.assign(schema.resolvers.Query, service.private.Query);
                }
                if(service.private.Mutation) {
                    schema.resolvers.Mutation = Object.assign(schema.resolvers.Mutation, service.private.Mutation);    
                }
                if(service.private.Types) {
                    Object.keys(service.private.Types).map((type) => {
                        schema.resolvers[type] = service.private.Types[type];
                    });
                }
                if(service.private.Dataloaders) {
                    Object.keys(service.private.Dataloaders).map((dlKey) => {
                        dataloader.defineLoader(dlKey, service.private.Dataloaders[dlKey]);
                    });
                }
            }

        }
    
    });

    return schema;
};

const buildSchema = (schemaObject) => {

    let schema = '';

    // Build types
        
    if(schemaObject.types.length > 0) {
        schema += `
            ${schemaObject.types}
        `;
    }

    // Build queries

    if(schemaObject.queries.length > 0) {
        schema += `
            type Query {
                ${schemaObject.queries}
            }
        `;
    }


    // Build Mutations

    if(schemaObject.mutations.length > 0) {
        schema += `
            type Mutation {
                ${schemaObject.mutations}
            }
        `;
    }

    return schema;
};

// Generate the schema object from your types definition.
module.exports.public = (dataloader) => {

    const schemaObject = getSchemaAndResolvers(false, dataloader);
    const schema = buildSchema(schemaObject);

    // console.log(schema);
    
    return makeExecutableSchema({typeDefs: schema, resolvers: schemaObject.resolvers});

};

module.exports.authenticated = (dataloader) => {

    const schemaObject = getSchemaAndResolvers(true, dataloader);
    const schema = buildSchema(schemaObject);

    // console.log('Authed schema', schema);
    console.log('finished executable building');
    return makeExecutableSchema({typeDefs: schema, resolvers: schemaObject.resolvers});

};