'use strict'

const auth0Service = require('../../../services/auth/auth0-service');

const schema = {
    types:`
        type Auth0TokenResponse {
            access_token: String
            expires_in: Int
            token_type: String
        }
    `,

    queries: `
        getToken(username: String!, password: String!): Auth0TokenResponse
    `
};

const getToken = (context, args) => {
    console.log('getToken params', args);
    return auth0Service.login(args.username, args.password)
        .then((result) => {
            console.log('getToken Result', result);
            return result;
        });
};

module.exports = {
    public: {
        Schema: schema,
        Query: {
            getToken
        }
    }
}