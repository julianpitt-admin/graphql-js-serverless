'use strict'

const trialService = require('../../../services/trial/trial-service');
const testService = require('../../../services/test/test-service');

const schema = {
    types:`
        type Trial {
            id: ID!
            name: String
            tests: [Test!]
        }
    `,

    queries: `
        getTrial(id: String!): Trial
        getAllTrials: [Trial!]!
    `,
        
    mutations: `
        createTrial(name: String!): Trial!
        updateTrial(id: String!, name: String!): Trial!
        removeTrial(id: String!): Trial
    `
};

const getTrial = (context, args) => {
    console.log('Get item params', args);
    return trialService.getTrial(args.id)
        .then((result) => {
            console.log('Get Trial Result', result);
            return result;
        });
};

const getAllTrials = () => {
    return trialService.getAllTrials()
        .then((result) => {
            console.log('Get all Trial Result', result);
            return result;
        });
};

const createTrial = (context, args) => {
    console.log('Create Trial Params', args);
    return trialService.createTrial(args)
        .then((result) => {
            console.log('Create Trial Result', result);
            return result;
        });
};

const removeTrial = (context, args) => {
    console.log('Remove Trial Params', args);
    return trialService.removeTrial(args.id)
        .then((result) => {
            console.log('Remove Trial Result', result);
            return result;
        });
};

const updateTrial = (context, args) => {
    console.log('Update Trial Params', args);
    return trialService.updateTrial(args)
        .then((result) => {
            console.log('Update Trial Result', result);
            return result;
        });
};

const Types = {
    Trial: {
        tests: (currentTrial) => testService.getTestsByTrialId(currentTrial.id)
    }
};

const Dataloaders = {
    getTrialById: trialId => {
        return trialService.getTrial(trialId[0]).then(res => {
            console.log('result of dataloaderfn', res);
            return [res];
        })
    }
};

module.exports = {
    public: {
        Schema: schema,
        Query: {
            getTrial,
            getAllTrials,
        },
        Mutation: {
            createTrial,
            removeTrial,
            updateTrial
        },
        Types: Types,
        Dataloaders: Dataloaders
    }
}