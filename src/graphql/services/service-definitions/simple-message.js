'use strict'

const lambdaInvoker = require('../../util/lambdaInvoker');

const publicSchema = {
    types:`
        type Simple {
            returnedMessage: String!
        }
    `,
    queries: `
        getSimpleMessage(message: String!): Simple!
    `,
};

const getSimpleMessage = (context, args) => {
    return lambdaInvoker('get-simple-message', { message: args.message }).then((resp) => {
        console.log('Lambda invoker response', resp);
        return resp;
    });
};

module.exports = {
    public: {
        Schema: publicSchema,
        Query: {
            getSimpleMessage
        }
    }
}