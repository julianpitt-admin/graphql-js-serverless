'use strict'

const testService = require('../../../services/test/test-service');

const publicSchema = {
    types:`
        type Test {
            id: ID!
            name: String
            description: String
            trial: Trial!
        }
    `,
    queries: `
        getAllTests: [Test!]!
    `,
};

const privateSchema = {
    mutations: `
        createTest(name: String!, description: String!, trialId: String!): Test!
        updateTest(id: String!, name: String!, description: String!, trialId: String!): Test!
        removeTest(id: String!): Test
    `,
    queries: `
        getTest(id: String!): Test
    `
};

const getTest = (context, args) => {
    console.log('Get item params', args);
    return testService.getTest(args.id)
        .then((result) => {
            console.log('Get Test Result', result);
            return result;
        });
};

const getAllTests = () => {
    console.log('Getting all tests');
    return testService.getAllTests()
        .then((result) => {
            console.log('Get all Test Result', result);
            return result;
        });
};

const createTest = (context, args) => {
    console.log('Create Test Params', args);
    return testService.createTest(args)
        .then((result) => {
            console.log('Create Test Result', result);
            return result;
        });
};

const removeTest = (context, args) => {
    console.log('Remove Test Params', args);
    return testService.removeTest(args.id)
        .then((result) => {
            console.log('Remove Test Result', result);
            return result;
        });
};

const updateTest = (context, args) => { 
    console.log('Update Test Params', args);
    return testService.updateTest(args)
        .then((result) => {
            console.log('Update Test Result', result);
            return result;
        });
};

const Types = {
    Test: {
        trial: (currentTest, data, { dataloader }) => {
            console.log('in type resolver getting ', 'getTrialById');
            return dataloader.getLoader('getTrialById').load(currentTest.trialId);
        }
    }
};

module.exports = {
    public: {
        Schema: publicSchema,
        Query: {
            getAllTests,
        }
    },
    private: {
        Schema: privateSchema,
        Query: {
            getTest
        },
        Mutation: {
            createTest,
            removeTest,
            updateTest
        },
        Types: Types
    }
}