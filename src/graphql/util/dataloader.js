'use strict'

const DataLoader = require('dataloader');

const loaders = {};

const getLoader = (loaderName) => {
  const loader = loaders[loaderName];
  if(!loader) {
    throw new Error(`Dataloader ${loaderName} is not defined`);
  }
  return loader;
};

const defineLoader = (loaderName, loaderFn) => {
  loaders[loaderName] = new DataLoader(loaderFn);
};

module.exports = {
  getLoader,
  defineLoader
};