'use strict'

const auth0Service = require('../../../services/auth/auth0-service');

const authorize = (event, config) => {
    return auth0Service.validate(event, config);
}

module.exports = {
    authorize
}