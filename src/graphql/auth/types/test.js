'use strict'


const authorize = (event, config) => {

    if(!event.headers.Authorization) {
        console.log('no auth header');
        return null;
    }

    const HEADER_REGEX = /bearer (.*)$/i;
    const bearerToken = HEADER_REGEX.exec(event.headers.Authorization);
    
    if(!(bearerToken && bearerToken.length && bearerToken.length >= 1)) {
        console.log('bearer token invalid');
        return null;
    }

    return {
        claims: {
            'something': 'something'
        }
    };

}

module.exports = {
    authorize
}