'use strict'

const authorizer = process.env.AUTH_PROVIDER;
const authConfig = process.env.AUTH_CONFIG;

module.exports.authorize = (event) => {
    
    // console.log('Selected authenticator', authorizer);
    // console.log('Selected authenticator config', JSON.stringify(authConfig, null, 2));
    
    const auth = require(`./types/${authorizer}.js`);
    return Promise.resolve(auth.authorize(event, authConfig));
}